# coding=utf-8
import scrapy
from urllib import request
import pymongo
from lxml import etree
from travelspider.items import TravelspiderItem
import redis
import os
import requests
import re
class TravelSpider(scrapy.Spider):
    name = 'travel_url'
    rds = redis.Redis(os.environ['REDISHOST'],int(os.environ['REDISPORT']))
    destination = str(os.environ["destination"])
    pages = int(os.environ["destination"])
    def start_requests(self):
        for index in range(1, pages):
            url = 'http://you.ctrip.com/travels/'+destination+'/t3-p%s.html'%index
            response = request.urlopen(url)
            page = response.read().decode('utf-8')
            urls = self.get_urls(page)
            if urls:
                for url in urls:
                    try:
                        rds.rpush(destionation,url)
                    except:
                        pass

    '''获取url列表'''
    def get_urls(self, content):
        selector = etree.HTML(content)
        urls = ['http://you.ctrip.com' + i for i in selector.xpath('//a[starts-with(@class,"journal-item")]/@href')]
        return set(urls)
