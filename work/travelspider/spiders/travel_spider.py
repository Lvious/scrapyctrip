# coding=utf-8
import scrapy
from urllib import request
import pymongo
from lxml import etree
from travelspider.items import TravelspiderItem
import redis
import os
import requests
import re
class TravelSpider(scrapy.Spider):
    name = 'travel'
    rds = redis.Redis(os.environ['REDISHOST'],int(os.environ['REDISPORT']))
    '''采集函数'''
    def start_requests(self):
        while True:
            try:
                url = self.rds.lpop(str(os.environ["destination"]))
                url = "http://you.ctrip.com"+url.decode("utf8")
                param = {'url': url}
                yield scrapy.Request(url=url, meta=param, callback=self.page_parser, dont_filter=True)
            except Exception as e:
                print(e)
                pass
    '''获取url列表'''
    def get_urls(self, content):
        selector = etree.HTML(content)
        urls = ['http://you.ctrip.com' + i for i in selector.xpath('//a[starts-with(@class,"journal-item")]/@href')]
        return set(urls)
    '''网页解析'''
    def page_parser(self, response):
        selector = etree.HTML(response.text)
        title = selector.xpath('//title/text()')[0]
        item = TravelspiderItem()
        item['url'] = response.meta['url']
        item['title'] = title
        item['raw'] = response.text
        yield item
        return
